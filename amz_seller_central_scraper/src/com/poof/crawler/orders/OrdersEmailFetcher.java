package com.poof.crawler.orders;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.druid.pool.DruidDataSource;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.poof.crawler.db.DBUtil;
import com.poof.crawler.utils.EncDecUtil;

/**
 * @author minglu
 * @mail minglu0302@gmail.com
 * @Date 2017年1月10日 下午4:26:41
 */
@Component
public class OrdersEmailFetcher {
	private static Logger log = Logger.getLogger(OrdersEmailFetcher.class);
	private static String ap_email;
	private static String ap_password;
	static {
		try {
			Properties p = new Properties();
			p.load(OrdersEmailFetcher.class.getResourceAsStream("/config.properties"));
			ap_email = p.getProperty("ap.email");
			ap_password = EncDecUtil.dec(p.getProperty("ap.password"));
		} catch (IOException e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}
	}

	public static boolean doLogin(WebClient webClient) throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		/** 1、打开amazom.com后台登录 */
		/*
		 * 我本机的，本来是正常，部署到poof生产环境报two step virifacation无法通过
		 * HtmlPage loginpage = webClient.getPage("https://sellercentral.amazon.com/ap/signin?openid.pape.max_auth_age=18000&openid.return_to=https%3A%2F%2Fsellercentral.amazon.com%2Fgp%2Fhomepage.html&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=sc_na_amazon_v2&_encoding=UTF8&openid.mode=checkid_setup&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&language=en_US&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&pageId=sc_na_amazon&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&ssoResponse=eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.QXjTgL8pF-5izIS4VTgT25Y4U0EDeCex9kH5BVs39JPHNPbkbBRyUA.C9V6M1HIbBGIX45K.zlzQnoK39kIodFC8D_30QOf5jo-J3WO_aBkZw_zeddaGE2zIb65D-Qqzcz6EEC0XLQo5x59wEVMTmp2wNEAII7Dy3Vu32tBuAtF6sLB9hmKN1XfiLMT4BuRMBZM4E6MBz4a71Ei-AZTCahqfHeejFs5oplueBzo8hneTgz-eGexMbVWMSNFauE8Vcf8VptCnBR4iDDFlD3gob2CiGbT9vLEbZK7GeURub_M9FGXoYJZzf5VK8JsU8UUuUa-LbTSrcga3.k46Dz6s4BH_onNu-bHMLcA");
		webClient.getCookieManager().clearCookies();
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "s_pers","%20s_dl%3D1%7C1499150226765%3B%20gpv_page%3DUS%253ASC%253A%2520SellerCentralLogin%7C1499150226787%3B"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id-time","2082787201l"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id","145-7633395-8563214"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "sid","\"KcIq/Bs2cE3dXwk1ruidoQ==|rFWTRT+oSt8LJ2PzDY4RWhyFBLmjkIw/ZYvTBZ3xefA=\""));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-token","\"6yEIy+7sUY0z2eK3s/nf2DKkB7XgvtXaP+w/afc866tapviPdO+xyS+I5tNbC8jCjMNUGC0WzGqSEqwtBaEVdfQxned+/4p23tfcTjkaPoufKOwNlimhkXTp3hYZ3GjB+3lSvL+0LtAzrlVn/1v6CFSkHzXTAMkbOAhsuGt6pAbSiiH8RUHus4Pt2Nu88f0i7q1coQIExDXasEtRY9QIZuNJJ84FQ7OlxZyn1d7Df9PeH6V1CNWeVU1FkU9K6DYI7yxv4mjRdVM=\""));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "ubid-main","134-2302692-9042118"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "at-main","Atza|IwEBIInJUE4NgQKz1L8qjTKZX16R5egLfn48_1q_MOXXc6dbLZhfujJdVGd7GcRs9IsJUeDom9R8HsoNJKUTFMBccJpsZhL_Kd0_3MUI-j5SY4CaOI8vxcq2WFKf259BvzUGni66b7AHyDUKvgRKKrPAhcjvXf90mGZLVfYpqPjaZb7d3NRjzasqI5lh2CnLDUiB7rthQSjKM9YoSXJmQ4dWsET_QPu_Eyymtv8awRjK0JDyrqFDiIy8DkdxbRxYSHOE1rxIlEy-LBnMnjB-8rR7YGQu50h2RzYt9fLOwAUt_0fl-RUEq3MPRa71NnGIVH4cwNKST9HoEtllni9M9mGZ_vYaN42r81hA_au6auFu8-oZu5WnLbRHgzuotPuBH12A0cjvIzYIOj3ixA4DT2HHxxB0wrlrF7j5fyMZt_0uCosw0Q"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "csm-hit","s-Y0C95TN7QKPX1DAQ5EXT|1499148429250"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "x-main","\"olXdbrh1goJIopXFl7DkxQJljrzVYcmtY9x?CNpMX6WsUS3SUNUt3Iys?gAROEej\""));
		webClient.getCookieManager().addCookie(new Cookie("account.amazon.com", "sso-cd-state","eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.yB4esHWGRUQYn69CaGZRKRNpHWSY9Hb3d9AIihwn9Nnzg2XtdmkIpQ.tArGlftgWFQkgryN.57qn600rKZW_0WwwgrkvbOIwBvsjN0kqg9xmVEx1cjZvp_ZULMIqkQeTErDjiWhzA7e0jDh-J_qJBtoLnc3LWgzA0WofmGczbagmSEPdmP33IiLmdJ5l-DSe9x9vBdBjosXkIFO2f4BAlRzv9B41wCN2bxzhkcDPowS4l5xOrA7EjQijv4Jn3lpbm7SaBdBF4vdCGvHg4n8X0lFh29t6pbDsJmRebPuy_XGdP_HsHWtuCPgxHcPit7QBkQOHC2hJkune016OCGv_HGqWZRHfoYMN-eyTeP_TiNb9FsD5HNdUKsA439w8lSU887TsFGQVM5tHsmal-hzZ11VwNtoJdOkf2RDeOnmKhJxbEzRuYviDNJqnzSc7z7ZuK3QdMk1KFcAkGZsEvQj7-Q95aHeg-MCtcfXWrpyxzQ6bUyCRDefn0RAVV8TJ8ogDgBGj6AjGYo7OYkI7Msyjsvo8D0r6Cw3xez3KH7yJ64Tpg-7sX_ZMvJXPy85UFydY7iqatlaOIAWibENcSrajSlqHaYhJ9pFowtSjAuLpj9MMgcS1V2H3g7SactY0uLryBvrnlsZs8Nftaa7Pmek7TrphOv-Q67YoL4pJyp8htJ_3yTJWMm02O42rY6nc8CHIXSVK_9BH6dH0S7_fOdOduxWxOdS3Fk6Z3HTzaugh09XZrxuMdlNkz7NtvRuSzFfHKusGguZnN6OehUFNK81Gvi9TYybe_hOIbVWXsSy4ecLw6KU2AR0Igj-REBn3PZhof1wzi4ui.XKMXUx4amzlHpuaONrX6_Q"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "signin-sso-state-us","121e33ec-d315-4eba-b8ce-996ef919b226"));
		*/

		HtmlPage loginpage = webClient.getPage("https://sellercentral.amazon.com/ap/signin?openid.pape.max_auth_age=18000&openid.return_to=https%3A%2F%2Fsellercentral.amazon.com%2Fgp%2Fhomepage.html&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=sc_na_amazon_v2&_encoding=UTF8&openid.mode=checkid_setup&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&language=en_US&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&pageId=sc_na_amazon&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&ssoResponse=eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.77w2BqrfjBHkUm1yoJLXldBl_Hx04GlgF3Whac9Mvou3AYtdKD2t4w.tkrNffSAXWh2_NWj.w9H_4VNg97A4-vEDkJ3Ub8zkE63zCy74Ovm9YkUR46LxMsL1jmiiSbNkMUp0qtxjbLpDqr6yKiaPWD6Bm7wsngtrXZt_j7eJSiEx2FlrEH9GgqA8qFHsf2vuN0ow-xVgvVR43N1nmL-xMxpj02mwG_vjRi2wKWvryLmut-6atevqCao2NOHBfTpd8qs_phBLP6tx5Dmma7BRvdBMxfKZieD8WzxV22Tm8pHTaW2-XLnAD0H7BktF2INb6Semssd90uXi.zauOIsKvvfaSKeq2tNs78Q");
		webClient.getCookieManager().clearCookies();
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "s_pers","%20s_ev15%3D%255B%255B%2527Typed/Bookmarked%2527%252C%25271499665979298%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499665984122%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499746012003%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499746014154%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499752983384%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499752984146%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499752990263%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499823123359%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499823124391%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499823125518%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271499823132056%2527%255D%255D%7C1657589532056%3B%20s_dl%3D1%7C1499825441124%3B%20gpv_page%3DUS%253ASC%253A%2520SellerCentralLogin%7C1499825441126%3B"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "s_sess","%20c_m%3DundefinedTyped/BookmarkedTyped/Bookmarked%3B%20s_cc%3Dtrue%3B%20s_sq%3D%3B"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id-time","2082787201l"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id","134-5491058-7429828"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "ubid-main","132-3811009-6507903"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "sid","\"+1+t4aXhd0zsDo6MgQozAw==|ktXWQprUPCCtSY4ZOzx842OuWQjQW013B4LxI8jTadw=\""));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-token","v9qMiZWSM+quWPliKI2zuYyfAmdtgZ/14NRFNV4BLQCimIB40eiPWuFonoTAeEUMfexQf89tM1glUsizkyrP4lHzoZxl6q87PXv+Pd0An5uEadqGF5thmNAhl++E7PH7XAAGNPhYPq3Ld99H1lRjTwxnjZPWN2yBWoN1GUyVyEz1WlnXb/vcQQ+W4XjOgPq8ReDngIeR/LlP+38egsSbLm1gMKsAxCW/i2lY26FO96/O7JW2GcoBFg=="));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "at-main","Atza|IwEBIInJUE4NgQKz1L8qjTKZX16R5egLfn48_1q_MOXXc6dbLZhfujJdVGd7GcRs9IsJUeDom9R8HsoNJKUTFMBccJpsZhL_Kd0_3MUI-j5SY4CaOI8vxcq2WFKf259BvzUGni66b7AHyDUKvgRKKrPAhcjvXf90mGZLVfYpqPjaZb7d3NRjzasqI5lh2CnLDUiB7rthQSjKM9YoSXJmQ4dWsET_QPu_Eyymtv8awRjK0JDyrqFDiIy8DkdxbRxYSHOE1rxIlEy-LBnMnjB-8rR7YGQu50h2RzYt9fLOwAUt_0fl-RUEq3MPRa71NnGIVH4cwNKST9HoEtllni9M9mGZ_vYaN42r81hA_au6auFu8-oZu5WnLbRHgzuotPuBH12A0cjvIzYIOj3ixA4DT2HHxxB0wrlrF7j5fyMZt_0uCosw0Q"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "x-wl-uid","1Qdag8FO9x1P8owfcSreiTeyiofkF1ZNOgxh0gM8FsQE8zRTxxD5+VfqAp54pGg+ZHVr7naAwcHtbV582VXQthe8xJPDJHnMGVDx2psJyzQyl+XhztVas322Oau1/28mnrEAn27SbTnI="));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "signin-sso-state-us","c88d6609-8b71-4026-848c-402f3f15bf58"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "csm-hit","487.39|1499823640983"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "fp_token_7c6a6574-f011-4c9a-abdd-9894a102ccef","\"+YVVhmTEHDhf4FJ6WB0zDUcGs3ZsqCfjHmgLmE+glFo=\""));
		webClient.getCookieManager().addCookie(new Cookie("account.amazon.com", "sso-cd-state","eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.K7ffM-PU6OAeDaD5WvT68cSxnpc8EVwji6GOSJl0xbk6jS04YzVpFg.y4g1w_QC2xZiIq_M.O1bBWo_yYSLLR5ZrMDBz6Il3LTRqcVsMpyIz5uHQWjoK1JDZJnXVpHhG1vZSqRNZJEFWiTUOcwj_6YCEm2iPCMyD_LA3XchXyZqnqf-KBSwOljalbG-Ust6saDygKOSmSF-lSx0Lq96nPGtdQ-JG7aL-GMsZuDTpvv7msULkD0Pl8sbp4v9Jhl_ZHSn9jptC9GE2W1fAGOed69E6ggN4VNg00ofKvaKlt0E5IDhyVsNlCyBZme9OQpZbaHdR1rqfLJ68qeOzmN-FZKFNHIR9iM9u_n6DmOAc7ci1VJaymn7QSTdk-HdMybtsfdikh9eGtoYgPfZuDZGp6F5mDb47ZZGtWy1d85wQvvA6ocXaKeI-P_bm2DgHQtUOGnrtxbuXjaksQ9QXfTN48DwfBaImZUcAv-yWgk9hVUPPPNdVaACScwL518dKEaWSMgjToCuPmjwMPfiZI0ck9Q9bvHvDhuNPutXNUM1Xo8qQKZbZdt_SeWYtvH5Jt5UOWGPXm-IuERf5Sds8vXMUVWSstDVJ-8r42Mp9GiIcKCFy-rS0WP1lwI6AIHpuSCJc6gSERK6X0QIjEke-NHYe_ip2i5rCNylAfmOF_ugZRz83CYwxdztgwBj9Oc2uAVjIiW6T5EiKuL4AbxsrKrUTWurt7H6OK3MrqvQPlWugaW9yEzAHxe-_jEQozA1h6Bvv3OgSl7mSnGmjVWBAGID90goyyGeC-IjFgKNu1QhdYBEjxcSQ6dV_qMlyTVa7bvdiz5lCbOHw5NU.d6speiSTQWslkFzdit1sAQ"));

		/** 2、输入帐号密码 */
		HtmlInput emailinput = (HtmlInput) loginpage.getHtmlElementById("ap_email");
		emailinput.setValueAttribute(ap_email);
		HtmlInput pwdinput = (HtmlInput) loginpage.getHtmlElementById("ap_password");
		pwdinput.setValueAttribute(ap_password);

		/** 3、点击登录按钮 */
		HtmlInput signbtn = (HtmlInput) loginpage.getHtmlElementById("signInSubmit");
		HtmlPage gphomepage = (HtmlPage) signbtn.click();
		if (null == gphomepage.getWebResponse() && gphomepage.getWebResponse().getStatusCode() != 200)
			return false;

		Set<Cookie> set = webClient.getCookieManager().getCookies();
		for (Iterator<Cookie> iterator = set.iterator(); iterator.hasNext();) {
			Cookie cookie = iterator.next();
			webClient.getCookieManager().addCookie(cookie);
			System.err.println(cookie);
		}
		return true;
	}

	/**
	 * BeiJing CST Time 2016/12/21 10:34 AM PST Time 2016/12/20 6:34 PM - 16H
	 * 默认设置为前两天
	 * @throws UnsupportedEncodingException 
	 */
	@Scheduled(cron = "0 0 04 1/7 * ?")
	public static void timer() throws UnsupportedEncodingException {

		WebClient webClient = new WebClient();
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setTimeout(60000);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getCookieManager().clearCookies();
		webClient.setAjaxController(new NicelyResynchronizingAjaxController());
		webClient.addRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
		webClient.addRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		webClient.addRequestHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
		webClient.addRequestHeader("referer", "http://www.google.com");
		webClient.addRequestHeader("Cache-Control", "no-cache");
		webClient.addRequestHeader("Connection", "keep-alive");
		webClient.getCookieManager().clearCookies();

		long startTime = System.currentTimeMillis();
		
		List<String> emails = new ArrayList<String>();
		Connection conn = null;
		try {
			conn = DBUtil.openConnection();
			emails = DBUtil.queryObjectList(conn, "select buyer_email as buyerEmail from bz_orders_sellercentral_buyeremail group by buyer_email", String.class);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		/** 1、登录 */
		while (true) {
			try {
				if (doLogin(webClient))
					break;
				else {
					log.error("caution! login failure, may the cookies was wrong or invaild !");
					return;
				}
			} catch (Exception e) {
				log.error(log.getName() + " find error : " + e);
				if (e instanceof SocketTimeoutException || e instanceof ConnectTimeoutException) {
					try {
						TimeUnit.SECONDS.sleep(new Random().nextInt(50) + 60);
					} catch (InterruptedException e1) {
					}
					continue;
				}
			}
		}

		/** 2、筛选订单 */
		/*
		 * preSelectedRange=1 取last day，前一天到当前所有
		 * 
		 * preSelectedRange=exactDates& 取指定时间范围内的 searchDateOption=exactDates&
		 * exactDateBegin=12%2F19%2F16& exactDateEnd=12%2F19%2F16
		 */
		for (String key : emails) {
			String orderUrl = "https://sellercentral.amazon.com/gp/orders-v2/list/ref=ag_myo_tnav_xx_?searchType=BuyerEmail&searchKeyword="
					+ URLEncoder.encode(key, "UTF-8")
					+ "&searchFulfillers=all&statusFilter=Default&orderType=All&site=sitesall&shippingServiceFilter=All&preSelectedRange=365&isDebug=&isSearch=1"
					+ "&itemsPerPage=100&query=&action=search-orders&serial=1&isBelowTheFold=1&ajaxBelowTheFoldRows=0&startTime="
					+ new Date().getTime()
					+ "&applicationPath=%2Fgp%2Forders-v2&currentPage=";
			
			/**
			 * ajaxBelowTheFoldRows=100 byDate=orderDate currentPage=1
			 * exactDateBegin=12%2F13%2F16 exactDateEnd=12%2F20%2F16
			 * highlightOrderID= isBelowTheFold=1 isDebug=0 isSearch=0
			 * itemsPerPage=100 paymentFilter=Default preSelectedRange=7
			 * searchDateOption=preSelected searchFulfillers=all searchKeyword=
			 * searchLanguage=en_US searchType=0 shipExactDateBegin=11%2F4%2F16
			 * shipExactDateEnd=12%2F27%2F16 shipSearchDateOption=shipPreSelected
			 * shipSelectedRange=7 shipmentFilter=Default showCancelled=0
			 * showPending=0 sortBy=OrderStatusDescending statusFilter=Default
			 */
			
			int maxpage = 1;
			List<String> list = new ArrayList<String>();
			for (int i = 1; i <= maxpage; i++) {

				int count = 1;
				while (true) {
					try {
						HtmlPage orderpage = webClient.getPage(orderUrl + i);
						
						String htmlContent = orderpage.getWebResponse().getContentAsString();
//						FileUtils.write(new File("d:\\index.html"), htmlContent, "utf-8");
						
						Document doc = Jsoup.parse(htmlContent);
						if (doc == null)
							return;
						
						if (maxpage == 1) {
							Elements pagelinks = doc.select(".tiny a.myo_list_orders_link");
							if (pagelinks.size() >= 3)
								maxpage = Integer.valueOf(pagelinks.eq(pagelinks.size() - 2).text().trim());
						}
						
						if (doc.select("tr[class*=order-row]").isEmpty()) {
							break;
						} else {
							Elements rows = doc.select("tr[class*=order-row]");
							for (Element row : rows) {
								String id = cleanOrderBlock(row);
								list.add(id);
							}
							// sleep thread
							TimeUnit.SECONDS.sleep(new Random().nextInt(50));
							break;
						}
					} catch (Exception e) {
						if (e instanceof SocketTimeoutException || e instanceof ConnectTimeoutException) {
							count ++;
							if(count > 5)
								break;
							
							try {
								TimeUnit.SECONDS.sleep(new Random().nextInt(50));
							} catch (InterruptedException e1) {
							}
							continue;
						} else {
							e.printStackTrace();
							log.error(log.getName() + " find error : " + e);
							break;
						}
					} finally {
						System.gc();
					}
				}
			}
			try {
				DBUtil.execute(conn, "update bz_orders_sellercentral_buyeremail set order_id = ? where buyer_email = ?", new Object[] { StringUtils.join(list, ","), key });
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					DBUtil.closeConnection();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		}
		long endTime = System.currentTimeMillis();
		System.err.println("order email done.");
		log.info(log.getName() + " : " + String.format("order email done，耗时%s秒", (endTime - startTime) / 1000));
		webClient.close();
	}

	private static String cleanOrderBlock(Element row) throws Exception {
		try {
			String id = row.select("input.order-id[type=hidden]").val();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}
		return "";
	}

	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
		DruidDataSource dataSource = (DruidDataSource) context.getBean("dataSource");
		DBUtil.setDataSource(dataSource);
		context.start();
		System.err.println("starting......");
		OrdersEmailFetcher.timer();
		System.in.read();
	}
}
