package com.poof.crawler.orders;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.druid.pool.DruidDataSource;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.poof.crawler.db.DBUtil;
import com.poof.crawler.utils.EncDecUtil;
import com.poof.crawler.utils.TimeUtil;

/**
 * @author minglu
 * @mail minglu0302@gmail.com
 * @Date 2017年1月10日 下午4:26:41
 */
@Component
public class OrdersFetcher_dl {
	private static Logger log = Logger.getLogger(OrdersFetcher_dl.class);
	private static String ap_email;
	private static String ap_password;
	static {
		try {
			Properties p = new Properties();
			p.load(OrdersFetcher_dl.class.getResourceAsStream("/config_dl.properties"));
			ap_email = p.getProperty("ap.email");
			ap_password = EncDecUtil.dec(p.getProperty("ap.password"));
		} catch (IOException e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}
	}

	public static boolean doLogin(WebClient webClient) throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		/** 1、打开amazom.com后台登录 */
		HtmlPage loginpage = webClient.getPage("https://sellercentral.amazon.com/ap/signin?openid.pape.max_auth_age=18000&openid.return_to=https%3A%2F%2Fsellercentral.amazon.com%2Fgp%2Fhomepage.html&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=sc_na_amazon_v2&_encoding=UTF8&openid.mode=checkid_setup&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&language=zh_CN&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&pageId=sc_na_amazon&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&ssoResponse=eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.zaZMzSMNZnyzi_W0qBO2X7KkezAgmUY9AgnSA7Pq6FPtXMEjX2Re8A.bJOT4f4gBlhsD-b8.gUkyQTUMmnikJ3r_jAot3piL_kED252TwOYJF2u0nv0wgGt7PFi5ixmP69R0aRnVKzwVs4HV1sRutoVOkdymwOxcklt-Ylk48TkXIMNIFNC3Pqw55LEp6qNAYvGdPmJl5YWgHC_iWgda8AmaYeiDZnD8I7whpOqJgasOLf49RjF_3lElrWPGyW39SePVkb7HHih7nIy6imMMcF0j-yTTFIEVaxvesR6yCXgD6MAM309NgDtxjTQ5CAUdquVonXFw7hzj.GG79cAfKdXosF2ZZ9dOGOg");
		webClient.getCookieManager().clearCookies();
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "s_pers","%20s_fid%3D4A445BC337E69F46-116E2BC03F6CF3FE%7C1564562095041%3B%20s_ev15%3D%255B%255B%2527Typed/Bookmarked%2527%252C%25271500869994927%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271500947981907%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271500948013391%2527%255D%252C%255B%2527NSOther%2527%252C%25271501223268019%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501486887286%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501638747246%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501638747943%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501638748085%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501638759604%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501638795690%2527%255D%252C%255B%2527Typed/Bookmarked%2527%252C%25271501638796157%2527%255D%255D%7C1659405196157%3B%20s_dl%3D1%7C1501640633335%3B%20gpv_page%3DUS%253ASC%253A%2520SellerCentralLogin%7C1501640633338%3B"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "s_sess","%20c_m%3DundefinedTyped/BookmarkedTyped/Bookmarked%3B%20s_cc%3Dtrue%3B%20s_sq%3Damznsrvsprod%252Camznsrvsmainprod%253D%252526pid%25253DUS%2525253ASC%2525253A%25252520SellerCentralLogin%252526pidt%25253D1%252526oid%25253Dhttps%2525253A//images-na.ssl-images-amazon.com/images/G/01/rainier/registration/sign-in-md-pri._CB394449286%252526ot%25253DIMAGE%3B"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id-time","2132358835l"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id","147-6724083-6822103"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "ubid-main","164-0392900-5052348"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-token","\"bEguL8YzvuUl4GS9SvwOG+AYyYRSdr3qXoqUjLXV6TV34bOaqJjpShcpjKf4iTtSS879FgiKuR4JjZzVUrW5fIOwkriyniQpnygR0VzJrMdxpjapr3lWkitMoZhPNEHVeHUx0nU+OAw3sOdDDgAJuJZ+CuoVkv4siVcBVuBJkc9aGmnfZPahv9Y22Hrl6MpP5KWQN3C+MgOQsPK6X01+bs4qdB0sR1qQAu/aRKkooR/XXwwD463AI/AONy0B8LkX7LyPUbIqPMo=\""));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "at-main","Atza|IwEBIJDQca1fekncuaZsC2h2c__rRhEJHdx52TKeQ0XXT6Cq962oGkFB5n8_rjK_AyijtTFEiaf3A2WQIBGdFuZQ8fGgerl_G2Z36GCpAP90fS4Ifh6gfBq4rAyrJC5yKByjzXR-MN0AjYUKPnw9oGr_eqCWR_oehZPnBB2p4PkJipYkGiSXAWhvuVOCJjXDidfOdCK4irvQxzLzyOo2tD8rooC8R_oIIP0l_I3zmGH8eVHv-hdkKbgPrd79tyc2z1FukbQhZKYRk_OQlkzTgNQdzRvResYMBR2vwNPkWbTvyoI7_yshVV4P1H8o-_Lb9y7zcamKTA0Sh3W7PhgJU7j6JvT6K1zC3uw8L5eHMwurGPddwauK20uHtTzM7-XTe9HarN19zs0JGJNdBiE74751UpEI"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "x-wl-uid","1IwD67aJ3sowbnyZk/9a3nLpE+LtNLur1kV7j10+/9lQDE30o1wMBPY7oG5CIMNiw6iwtJgFAn56xOwsL11nGR7KaLcMZxior2KNAmuqJiFoH5WsobGTFQ/rjIY9XiCQk5mvYeIhCTimC5X6arjJrEg=="));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "signin-sso-state-us","075d48eb-89a6-496f-8b80-ccd0bdaf2211"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "csm-hit","s-RXX94DERBA2CBWC48CMF|1501638836009"));
		webClient.getCookieManager().addCookie(new Cookie("account.amazon.com", "sso-cd-state","eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.dvlx0lpWMdrMnfN2PuXvsq-EbGC4_OGVJzLXRxBGT2X1CyZw4fRD_Q.XtvlaaU3vnyU-h8r.pPtm64I8KlJs07L2r7lJmrujro9SkP7S0Oy-jpYYq7eLXrjFrkNTSuJMgwbxygEvLoj4irYz0oS1A0KHvGoUk_n1j-i2Srysb2p-MyXKgg4JQcq9aTG0HhqYAcTFTop7fIg0X2N98dQBKLBA4lMZtATEapp4fQqD7kaJ_uqkFLsUpuxs9CWTC4arP4nJILnxUkW7osu2knhNWTgF6kALXbBsssTFBBtaQVXNe0Dk4wxE4hXpa-w2xzhPRIdozKuIE9wbGrekFZxOerGx_r1AJZ8pI62fk9I56F6uEWY1JpAoYGfdXKOxuGkl-6eASSMRSyVFu0n0CE2jwWux73aUPeJ8Q2kcnfEcW1RN4Khf7qewfDZdCAKQqVkM1BKIoHYWMuYhbOA014aQjbijcDCkcSomO7n0VwrZdeiPWxru-Kl_lzLh6lofJNhPVcOD-LmAcix29Fi96qvouSSYwliNPKzNnkTHZw7Vt7q7v5sfBCAx-ZIbZzEJRw6pk5sHkKLBqBr7P9S3R6WOEnjEJaGZe7g8TskiZ50GAuRpcj0lD2yqW1-RKFJBVlijizo8OUZlwRbIoTNYbJEltVLz0c7fHhR2l7QG1aM5Hd3wlQ8WeswA7Q5uwTvkFudnR3d-I2nZybBlQPyKe7E2_hevRiWK2zebraC9S1nD5-AAzfa4AZx8yu48WN8KIgtu3VJATivcUg9SbI0g_F9Ei1VU5sNJlB_3g1EVVPe5bE7a9-O3vv0.mHGR2GTCnF4pESPNnazUFQ"));


		/** 2、输入帐号密码 */
		HtmlInput emailinput = (HtmlInput) loginpage.getHtmlElementById("ap_email");
		emailinput.setValueAttribute(ap_email);
		HtmlInput pwdinput = (HtmlInput) loginpage.getHtmlElementById("ap_password");
		pwdinput.setValueAttribute(ap_password);

		/** 3、点击登录按钮 */
		HtmlInput signbtn = (HtmlInput) loginpage.getHtmlElementById("signInSubmit");
		HtmlPage gphomepage = (HtmlPage) signbtn.click();
		
		FileUtils.write(new File("d:\\1.html"), gphomepage.getWebResponse().getContentAsString(), "utf-8");
		
		if (null == gphomepage.getWebResponse() && gphomepage.getWebResponse().getStatusCode() != 200)
			return false;

		Set<Cookie> set = webClient.getCookieManager().getCookies();
		for (Iterator<Cookie> iterator = set.iterator(); iterator.hasNext();) {
			Cookie cookie = iterator.next();
			webClient.getCookieManager().addCookie(cookie);
			System.err.println(cookie);
		}
		return true;
	}

	/**
	 * BeiJing CST Time 2016/12/21 10:34 AM PST Time 2016/12/20 6:34 PM - 16H
	 * 默认设置为前两天
	 */
	@Scheduled(cron = "0 0 23 * * ?")
	public static void timer() {

		WebClient webClient = new WebClient();
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setTimeout(60000);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getCookieManager().clearCookies();
		webClient.setAjaxController(new NicelyResynchronizingAjaxController());
		webClient.addRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
		webClient.addRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		webClient.addRequestHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
		webClient.addRequestHeader("referer", "http://www.google.com");
		webClient.getCookieManager().clearCookies();

		long startTime = System.currentTimeMillis();
		try {

			/** 1、登录 */
			if (!doLogin(webClient))
				throw new IllegalArgumentException("do login error");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}

		/** 2、筛选订单 */
		/*
		 * preSelectedRange=1 取last day，前一天到当前所有
		 * 
		 * preSelectedRange=exactDates& 取指定时间范围内的 searchDateOption=exactDates&
		 * exactDateBegin=12%2F19%2F16& exactDateEnd=12%2F19%2F16
		 */


		/**
		 * ajaxBelowTheFoldRows=100 byDate=orderDate currentPage=1
		 * exactDateBegin=12%2F13%2F16 exactDateEnd=12%2F20%2F16
		 * highlightOrderID= isBelowTheFold=1 isDebug=0 isSearch=0
		 * itemsPerPage=100 paymentFilter=Default preSelectedRange=7
		 * searchDateOption=preSelected searchFulfillers=all searchKeyword=
		 * searchLanguage=en_US searchType=0 shipExactDateBegin=11%2F4%2F16
		 * shipExactDateEnd=12%2F27%2F16 shipSearchDateOption=shipPreSelected
		 * shipSelectedRange=7 shipmentFilter=Default showCancelled=0
		 * showPending=0 sortBy=OrderStatusDescending statusFilter=Default
		 */
		
		Calendar start = Calendar.getInstance();
		start.set(2017, 6, 28);
		Long startTIme = start.getTimeInMillis();

		Calendar end = Calendar.getInstance();
		end.setTime(TimeUtil.formatTimeZone(TimeZone.getTimeZone("PST"), 1)); // 设置为前1天
		Long endTime = end.getTimeInMillis();

		Long oneDay = 1000 * 60 * 60 * 24l;

		Long time = startTIme;
		HtmlPage orderpage = null;
		while (time <= endTime) {
			Date d = new Date(time);
			String date = (d.getMonth() + 1) + "%2F" + (d.getDate()) + "%2F" + ((d.getYear() + 1900) + "").substring(2);

			String orderUrl = "https://sellercentral.amazon.com/gp/orders-v2/list/ref=ag_myo_tnav_xx_?preSelectedRange=exactDates&searchDateOption=exactDates&" + "exactDateBegin=" + date + "&"
					+ "exactDateEnd=" + date + "&" + "itemsPerPage=100&showPending=1&isBelowTheFold=1&sortBy=OrderDateDescending&ajaxBelowTheFoldRows=0&currentPage=";

			int maxpage = 1;
			for (int i = 1; i <= maxpage; i++) {
				try {
	
					orderpage = webClient.getPage(orderUrl + i);
	
					String htmlContent = orderpage.getWebResponse().getContentAsString("utf-8");
//					if (htmlContent.contains("Robot ddd")) {
//						FileUtils.write(new File("d:\\robot.html"), htmlContent,"utf-8");
//						throw new IllegalArgumentException("Robot Check !!!  Repeat, Robot Check !!!");
//					}
	
					Document doc = Jsoup.parse(htmlContent);
					if (doc == null)
						return;
	
					 FileUtils.write(new File("d:\\1.html"), doc.html(),"utf-8");
					if (maxpage == 1) {
						Elements pagelinks = doc.select(".tiny a.myo_list_orders_link");
						if (pagelinks.size() >= 3)
							maxpage = Integer.valueOf(pagelinks.eq(pagelinks.size() - 2).text().trim());
					}
	
					if (doc.select("tr[class*=order-row]").isEmpty()) {
						continue;
					} else {
						List<Orders> list = new ArrayList<Orders>();
						Elements rows = doc.select("tr[class*=order-row]");
						for (Element row : rows) {
							Orders order = cleanOrderBlock(row);
							list.add(order);
						}
	
						BatchInsert(list);
	
						// sleep thread
						TimeUnit.SECONDS.sleep(new Random().nextInt(10) + 5);
					}
				} catch (Exception e) {
					e.printStackTrace();
					log.error(log.getName() + " : program error: " + e);
				} finally {
					System.gc();
				}
			}
			time += oneDay;
			System.err.println(date.replace("%20F", "-"));
		}
		long endSTime = System.currentTimeMillis();
		System.err.println("sellercentral orders done.");
		log.info(log.getName() + " : " + String.format("sellercentral orders done，耗时%s秒", (endSTime - startTime) / 1000));
		webClient.close();
	}

	/**
	 * save to db 1、爬虫采集数据（只采集前一天） 2、清洗数据 Transaction( 3、删除前一天的数据 4、插入新数据 )
	 */
	private static void BatchInsert(List<Orders> orders) throws Exception {
		String sql = "insert into bz_orders_sellercentral (order_id, asin, sku, qty, title, es_level, shipped, shipping, buyer_id, buyer_name, merch_fulfilled, marketplace_id, latest_ship_date, is_prime, number_of_items_remaining_toship, order_date, status)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		log.info(log.getName() + " : fetching sellercentral size " + orders.size());
		try {
			conn = DBUtil.openConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
			PreparedStatement pstmt = conn.prepareStatement(sql);
			int size = orders.size() / 20;
			size = orders.size() % 20 >= 0 ? size + 1 : size; // 5521,5
			for (int i = 0; i < size; i++) { // 6
				for (int j = 0; j < (i == size - 1 ? orders.size() % 20 : 20); j++) {
					Orders bean = orders.get(i * 20 + j);
					pstmt.setString(1, bean.getId());
					pstmt.setString(2, bean.getAsin());
					pstmt.setString(3, bean.getSku());
					pstmt.setInt(4, bean.getQty());
					pstmt.setString(5, bean.getTitle());
					pstmt.setString(6, bean.getEsLevel());
					pstmt.setBoolean(7, bean.isShipped());
					pstmt.setString(8, bean.getShipping());
					pstmt.setString(9, bean.getBuyerId());
					pstmt.setString(10, bean.getBuyerName());
					pstmt.setBoolean(11, bean.isMerchFulfilled());
					pstmt.setString(12, bean.getMarketplaceId());
					pstmt.setString(13, bean.getLatestShipDate());
					pstmt.setBoolean(14, bean.isPrime());
					pstmt.setInt(15, bean.getNumberOfItemsRemainingToShip());
					pstmt.setString(16, bean.getOrderDate());
					pstmt.setString(17, bean.getStatus());

					pstmt.addBatch();
				}
				pstmt.executeBatch();
				pstmt.clearBatch();
			}
			if (size > 0) {
			}
			conn.commit();
			pstmt.close();
		} catch (Exception e) {
			log.error(log.getName() + " : program error: " + e);
			throw e;
		} finally {
			DBUtil.closeConnection();
		}
	}

	private static Orders cleanOrderBlock(Element row) throws Exception {
		try {
			String id = row.select("input.order-id[type=hidden]").val();
			String eslevel = row.select("input.es-level[type=hidden]").isEmpty() ? "" : row.select("input.es-level[type=hidden]").val();
			boolean shipped = (row.select("input.num-shipped[type=hidden]") == null || "0".equals(row.select("input.num-shipped[type=hidden]").val())) ? false : true;
			String buyerId = row.select("input.cust-id[type=hidden]").isEmpty() ? "" : row.select("input.cust-id[type=hidden]").val();
			boolean merchFulfilled = (row.select("input.merch-fulfilled[type=hidden]") == null || "0".equals(row.select("input.merch-fulfilled[type=hidden]").val())) ? false : true;
			String marketplaceId = row.select("input.marketplace-id[type=hidden]").isEmpty() ? "" : row.select("input.marketplace-id[type=hidden]").val();
			String latestShipDate = row.select("input.latestShipDate[type=hidden]").isEmpty() ? "" : row.select("input.latestShipDate[type=hidden]").val();
			latestShipDate = StringUtils.isNotBlank(latestShipDate) ? new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(Long.valueOf(latestShipDate) * 1000)) : "";
			boolean isPrime = (row.select("input.isPrime[type=hidden]") == null || "0".equals(row.select("input.isPrime[type=hidden]").val())) ? false : true;
			String numberOfItemsRemainingToShip = row.select("input.numberOfItemsRemainingToShip[type=hidden]").isEmpty() ? null : row.select("input.numberOfItemsRemainingToShip[type=hidden]").val();

			String orderDate = row.select("td").isEmpty() ? "" : row.select("td").eq(1).text();
			String title = row.select("td div[id*=orderItem] span[id*=product]").isEmpty() ? "" : row.select("td div[id*=orderItem] span[id*=product]").text();
			Elements qtyEle = row.select("td div[id*=orderItem] td:contains(QTY)").isEmpty() ? null : row.select("td div[id*=orderItem] td:contains(QTY)");
			int qty = 0;
			for (Element tmp : qtyEle) {
				qty += tmp != null ? Integer.valueOf(tmp.text().replaceAll("[^\\d.]", "")) : 0;
			}
			String asin = row.select("td div[id*=orderItem] td:contains(ASIN)").isEmpty() ? "" : row.select("td div[id*=orderItem] td:contains(ASIN)").text().replaceAll("ASIN:", "").trim();
			String sku = row.select("td div[id*=orderItem] td:contains(SKU)").isEmpty() ? "" : row.select("td div[id*=orderItem] td:contains(SKU)").text().replaceAll("SKU:", "").trim();
			String buyerName = row.select("td a[id*=buyerName]").isEmpty() ? "" : row.select("td a[id*=buyerName]").text();
			String shipping = row.select("td[class*=order-cell]").isEmpty() ? "" : row.select("td[class*=order-cell]").eq(3).text();
			String status = row.select("td[class*=order-cell]").isEmpty() ? "" : row.select("td[class*=order-cell]").eq(4).select("div:not([style=display:none])").text();
			return new Orders(id, asin, sku, qty, title, eslevel, shipped, shipping, buyerId, buyerName, merchFulfilled, marketplaceId, latestShipDate, isPrime,
					Integer.valueOf(numberOfItemsRemainingToShip), orderDate, status);

		} catch (Exception e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
		DruidDataSource dataSource = (DruidDataSource) context.getBean("dataSource");
		DBUtil.setDataSource(dataSource);
		context.start();
		System.err.println("starting......");
		OrdersFetcher_dl.timer();
		System.in.read();
	}
}
