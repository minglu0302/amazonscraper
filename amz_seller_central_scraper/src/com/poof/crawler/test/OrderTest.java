package com.poof.crawler.test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.druid.pool.DruidDataSource;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.poof.crawler.db.DBUtil;
import com.poof.crawler.orders.Orders;
import com.poof.crawler.utils.EncDecUtil;
import com.poof.crawler.utils.TimeUtil;

/**
 * @author minglu
 * @mail minglu0302@gmail.com
 * @Date 2017年1月10日 下午4:26:41
 */
@Component
public class OrderTest {
	private static Logger log = Logger.getLogger(OrderTest.class);
	private static String ap_email;
	private static String ap_password;
	static {
		try {
			Properties p = new Properties();
			p.load(OrderTest.class.getResourceAsStream("/config.properties"));
			ap_email = p.getProperty("ap.email");
			ap_password = EncDecUtil.dec(p.getProperty("ap.password"));
		} catch (IOException e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}
	}
	public static boolean doLogin(WebClient webClient) throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		/** 1、打开amazom.com后台登录 */
		HtmlPage loginpage = webClient.getPage("https://sellercentral.amazon.com/ap/signin?openid.pape.max_auth_age=18000&openid.return_to=https%3A%2F%2Fsellercentral.amazon.com%2Fgp%2Fhomepage.html&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=sc_na_amazon_v2&_encoding=UTF8&openid.mode=checkid_setup&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&language=en_US&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&pageId=sc_na_amazon&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&ssoResponse=eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.QXjTgL8pF-5izIS4VTgT25Y4U0EDeCex9kH5BVs39JPHNPbkbBRyUA.C9V6M1HIbBGIX45K.zlzQnoK39kIodFC8D_30QOf5jo-J3WO_aBkZw_zeddaGE2zIb65D-Qqzcz6EEC0XLQo5x59wEVMTmp2wNEAII7Dy3Vu32tBuAtF6sLB9hmKN1XfiLMT4BuRMBZM4E6MBz4a71Ei-AZTCahqfHeejFs5oplueBzo8hneTgz-eGexMbVWMSNFauE8Vcf8VptCnBR4iDDFlD3gob2CiGbT9vLEbZK7GeURub_M9FGXoYJZzf5VK8JsU8UUuUa-LbTSrcga3.k46Dz6s4BH_onNu-bHMLcA");
		webClient.getCookieManager().clearCookies();
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "s_pers","%20s_dl%3D1%7C1499150226765%3B%20gpv_page%3DUS%253ASC%253A%2520SellerCentralLogin%7C1499150226787%3B"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id-time","2082787201l"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-id","145-7633395-8563214"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "sid","\"KcIq/Bs2cE3dXwk1ruidoQ==|rFWTRT+oSt8LJ2PzDY4RWhyFBLmjkIw/ZYvTBZ3xefA=\""));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "session-token","\"6yEIy+7sUY0z2eK3s/nf2DKkB7XgvtXaP+w/afc866tapviPdO+xyS+I5tNbC8jCjMNUGC0WzGqSEqwtBaEVdfQxned+/4p23tfcTjkaPoufKOwNlimhkXTp3hYZ3GjB+3lSvL+0LtAzrlVn/1v6CFSkHzXTAMkbOAhsuGt6pAbSiiH8RUHus4Pt2Nu88f0i7q1coQIExDXasEtRY9QIZuNJJ84FQ7OlxZyn1d7Df9PeH6V1CNWeVU1FkU9K6DYI7yxv4mjRdVM=\""));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "ubid-main","134-2302692-9042118"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "at-main","Atza|IwEBIInJUE4NgQKz1L8qjTKZX16R5egLfn48_1q_MOXXc6dbLZhfujJdVGd7GcRs9IsJUeDom9R8HsoNJKUTFMBccJpsZhL_Kd0_3MUI-j5SY4CaOI8vxcq2WFKf259BvzUGni66b7AHyDUKvgRKKrPAhcjvXf90mGZLVfYpqPjaZb7d3NRjzasqI5lh2CnLDUiB7rthQSjKM9YoSXJmQ4dWsET_QPu_Eyymtv8awRjK0JDyrqFDiIy8DkdxbRxYSHOE1rxIlEy-LBnMnjB-8rR7YGQu50h2RzYt9fLOwAUt_0fl-RUEq3MPRa71NnGIVH4cwNKST9HoEtllni9M9mGZ_vYaN42r81hA_au6auFu8-oZu5WnLbRHgzuotPuBH12A0cjvIzYIOj3ixA4DT2HHxxB0wrlrF7j5fyMZt_0uCosw0Q"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "csm-hit","s-Y0C95TN7QKPX1DAQ5EXT|1499148429250"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "x-main","\"olXdbrh1goJIopXFl7DkxQJljrzVYcmtY9x?CNpMX6WsUS3SUNUt3Iys?gAROEej\""));
		webClient.getCookieManager().addCookie(new Cookie("account.amazon.com", "sso-cd-state","eyJ6aXAiOiJERUYiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiQTI1NktXIn0.yB4esHWGRUQYn69CaGZRKRNpHWSY9Hb3d9AIihwn9Nnzg2XtdmkIpQ.tArGlftgWFQkgryN.57qn600rKZW_0WwwgrkvbOIwBvsjN0kqg9xmVEx1cjZvp_ZULMIqkQeTErDjiWhzA7e0jDh-J_qJBtoLnc3LWgzA0WofmGczbagmSEPdmP33IiLmdJ5l-DSe9x9vBdBjosXkIFO2f4BAlRzv9B41wCN2bxzhkcDPowS4l5xOrA7EjQijv4Jn3lpbm7SaBdBF4vdCGvHg4n8X0lFh29t6pbDsJmRebPuy_XGdP_HsHWtuCPgxHcPit7QBkQOHC2hJkune016OCGv_HGqWZRHfoYMN-eyTeP_TiNb9FsD5HNdUKsA439w8lSU887TsFGQVM5tHsmal-hzZ11VwNtoJdOkf2RDeOnmKhJxbEzRuYviDNJqnzSc7z7ZuK3QdMk1KFcAkGZsEvQj7-Q95aHeg-MCtcfXWrpyxzQ6bUyCRDefn0RAVV8TJ8ogDgBGj6AjGYo7OYkI7Msyjsvo8D0r6Cw3xez3KH7yJ64Tpg-7sX_ZMvJXPy85UFydY7iqatlaOIAWibENcSrajSlqHaYhJ9pFowtSjAuLpj9MMgcS1V2H3g7SactY0uLryBvrnlsZs8Nftaa7Pmek7TrphOv-Q67YoL4pJyp8htJ_3yTJWMm02O42rY6nc8CHIXSVK_9BH6dH0S7_fOdOduxWxOdS3Fk6Z3HTzaugh09XZrxuMdlNkz7NtvRuSzFfHKusGguZnN6OehUFNK81Gvi9TYybe_hOIbVWXsSy4ecLw6KU2AR0Igj-REBn3PZhof1wzi4ui.XKMXUx4amzlHpuaONrX6_Q"));
		webClient.getCookieManager().addCookie(new Cookie("sellercentral.amazon.com", "signin-sso-state-us","121e33ec-d315-4eba-b8ce-996ef919b226"));
		
		/** 2、输入帐号密码 */
		HtmlInput emailinput = (HtmlInput) loginpage.getHtmlElementById("ap_email");
		emailinput.setValueAttribute(ap_email);
		HtmlInput pwdinput = (HtmlInput) loginpage.getHtmlElementById("ap_password");
		pwdinput.setValueAttribute(ap_password);

		/** 3、点击登录按钮 */
		HtmlInput signbtn = (HtmlInput) loginpage.getHtmlElementById("signInSubmit");
		HtmlPage gphomepage = (HtmlPage) signbtn.click();
		
		FileUtils.write(new File("d:\\1.html"), gphomepage.getWebResponse().getContentAsString(), "utf-8");
		
		if (null == gphomepage.getWebResponse() && gphomepage.getWebResponse().getStatusCode() != 200)
			return false;
		
		Set<Cookie> set = webClient.getCookieManager().getCookies();
		for (Iterator<Cookie> iterator = set.iterator(); iterator.hasNext();) {
			Cookie cookie = iterator.next();
			webClient.getCookieManager().addCookie(cookie);
			System.err.println(cookie);
		}
		return true;
	}

	/**
	 *  BeiJing CST Time 2016/12/21 10:34 AM
	 *  PST	Time				2016/12/20 6:34 PM
	 *  - 16H
	 *  默认设置为前两天
	 */
	@Scheduled(cron = "0 0 23 * * ?")
	public static void timer() {

		WebClient webClient = new WebClient();
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setTimeout(60000);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getCookieManager().clearCookies();
		webClient.setAjaxController(new NicelyResynchronizingAjaxController());
		webClient.addRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
		webClient.addRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		webClient.addRequestHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		webClient.addRequestHeader("Accept-Language", "zh-CN,zh;q=0.8");
		webClient.addRequestHeader("referer", "http://www.google.com");

		long startTime = System.currentTimeMillis();
		try {

			/** 1、登录 */
			if (!doLogin(webClient))
				throw new IllegalArgumentException("do login error");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}

			/** 2、筛选订单*/
			/* preSelectedRange=1					取last day，前一天到当前所有
			 * 
			 * preSelectedRange=exactDates&	取指定时间范围内的
			 * searchDateOption=exactDates&
			 * exactDateBegin=12%2F19%2F16&
			 * exactDateEnd=12%2F19%2F16
			 */
			

			Calendar c = Calendar.getInstance();
			c.setTime(TimeUtil.formatTimeZone(TimeZone.getTimeZone("PST"), 1)); // 设置为前1天
			Calendar from = Calendar.getInstance();
			from.set(2017, 6, 1);
			int month = c.get(Calendar.MONTH) + 1;
			for (int cur = 6; cur < month; cur++) {
				from.set(Calendar.MONTH, cur);
				String begindate = (from.get(Calendar.MONTH) + 1) + "%2F" + (from.get(Calendar.DATE)) + "%2F" + (from.get(Calendar.YEAR) + "").substring(2);
				if (cur == c.get(Calendar.MONTH))
					from.set(Calendar.DATE, c.get(Calendar.DATE));
				else
					from.set(Calendar.MONTH, from.get(Calendar.MONTH)+1);
				String enddate = (from.get(Calendar.MONTH) + 1) + "%2F" + (from.get(Calendar.DATE)) + "%2F" + (from.get(Calendar.YEAR) + "").substring(2);
				
				String orderUrl = "https://sellercentral.amazon.com/gp/orders-v2/list/ref=ag_myo_tnav_xx_?preSelectedRange=exactDates&searchDateOption=exactDates&"
						+ "exactDateBegin=" + begindate + "&"
						+ "exactDateEnd=" + enddate + "&"
						+ "itemsPerPage=100&showPending=1&isBelowTheFold=1&sortBy=OrderDateDescending&ajaxBelowTheFoldRows=0&currentPage=";
				
				/**
				 * ajaxBelowTheFoldRows=100 byDate=orderDate currentPage=1
				 * exactDateBegin=12%2F13%2F16 exactDateEnd=12%2F20%2F16
				 * highlightOrderID= isBelowTheFold=1 isDebug=0 isSearch=0
				 * itemsPerPage=100 paymentFilter=Default preSelectedRange=7
				 * searchDateOption=preSelected searchFulfillers=all searchKeyword=
				 * searchLanguage=en_US searchType=0 shipExactDateBegin=11%2F4%2F16
				 * shipExactDateEnd=12%2F27%2F16
				 * shipSearchDateOption=shipPreSelected shipSelectedRange=7
				 * shipmentFilter=Default showCancelled=0 showPending=0
				 * sortBy=OrderStatusDescending statusFilter=Default
				 */
				
				int maxpage = 1;
				for (int i = 1; i <= maxpage; i++) {
					try {
	
						HtmlPage orderpage = webClient.getPage(orderUrl + i);
	
						String htmlContent = orderpage.getWebResponse().getContentAsString();
	
						Document doc = Jsoup.parse(htmlContent);
						if (doc == null)
							return;
	
					FileUtils.write(new File("d:\\1.html"), doc.html(), "utf-8");
						if (maxpage == 1) {
							Elements pagelinks = doc.select(".tiny a.myo_list_orders_link");
							if (pagelinks.size() >= 3)
								maxpage = Integer.valueOf(pagelinks.eq(pagelinks.size() - 2).text().trim());
						}
	
						if (doc.select("tr[class*=order-row]").isEmpty()) {
							continue;
						} else {
							List<Orders> list = new ArrayList<Orders>();
							Elements rows = doc.select("tr[class*=order-row]");
							for (Element row : rows) {
								Orders order = cleanOrderBlock(row);
								list.add(order);
							}
	
							BatchInsert(list);
	
							// sleep thread
							TimeUnit.SECONDS.sleep(new Random().nextInt(5));
						}
					} catch (Exception e) {
						e.printStackTrace();
						log.error(log.getName() + " : program error: " + e);
					} finally {
//						webClient.close();
						System.gc();
					}
				}
			}
		long endTime = System.currentTimeMillis();
		System.err.println("sellercentral orders done.");
		log.info(log.getName() + " : " + String.format("sellercentral orders done，耗时%s秒", (endTime - startTime) / 1000));
		webClient.close();
	}

	/**
	 * save to db
	 * 1、爬虫采集数据（只采集前一天）
	 * 2、清洗数据
	 * Transaction(
	 * 3、删除前一天的数据
	 * 4、插入新数据
	 * )
	 */
	private static void BatchInsert(List<Orders> orders) throws Exception {
		String sql = "insert into bz_orders_sellercentral (order_id, asin, sku, qty, title, es_level, shipped, shipping, buyer_id, buyer_name, merch_fulfilled, marketplace_id, latest_ship_date, is_prime, number_of_items_remaining_toship, order_date, status)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		log.info(log.getName() + " : fetching sellercentral size " + orders.size());
		try {
			conn = DBUtil.openConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
			PreparedStatement pstmt = conn.prepareStatement(sql);
			int size = orders.size() / 20;
			size = orders.size() % 20 >= 0 ? size + 1 : size; // 5521,5
			for (int i = 0; i < size; i++) { // 6
				for (int j = 0; j < (i == size - 1 ? orders.size() % 20 : 20); j++) {
					Orders bean = orders.get(i * 20 + j);
					pstmt.setString(1, bean.getId());
					pstmt.setString(2, bean.getAsin());
					pstmt.setString(3, bean.getSku());
					pstmt.setInt(4, bean.getQty());
					pstmt.setString(5, bean.getTitle());
					pstmt.setString(6, bean.getEsLevel());
					pstmt.setBoolean(7, bean.isShipped());
					pstmt.setString(8, bean.getShipping());
					pstmt.setString(9, bean.getBuyerId());
					pstmt.setString(10, bean.getBuyerName());
					pstmt.setBoolean(11, bean.isMerchFulfilled());
					pstmt.setString(12, bean.getMarketplaceId());
					pstmt.setString(13, bean.getLatestShipDate());
					pstmt.setBoolean(14, bean.isPrime());
					pstmt.setInt(15, bean.getNumberOfItemsRemainingToShip());
					pstmt.setString(16, bean.getOrderDate());
					pstmt.setString(17, bean.getStatus());

					pstmt.addBatch();
				}
				pstmt.executeBatch();
				pstmt.clearBatch();
			}
			if (size > 0) {
			}
			conn.commit();
			pstmt.close();
		} catch (Exception e) {
			log.error(log.getName() + " : program error: " + e);
			throw e;
		} finally {
			DBUtil.closeConnection();
		}
	}

	private static Orders cleanOrderBlock(Element row) throws Exception {
		try {
			String id = row.select("input.order-id[type=hidden]").val();
			String eslevel = row.select("input.es-level[type=hidden]").isEmpty() ? "" : row.select("input.es-level[type=hidden]").val();
			boolean shipped = (row.select("input.num-shipped[type=hidden]") == null || "0".equals(row.select("input.num-shipped[type=hidden]").val())) ? false : true;
			String buyerId = row.select("input.cust-id[type=hidden]").isEmpty() ? "" : row.select("input.cust-id[type=hidden]").val();
			boolean merchFulfilled = (row.select("input.merch-fulfilled[type=hidden]") == null || "0".equals(row.select("input.merch-fulfilled[type=hidden]").val())) ? false : true;
			String marketplaceId = row.select("input.marketplace-id[type=hidden]").isEmpty() ? "" : row.select("input.marketplace-id[type=hidden]").val();
			String latestShipDate = row.select("input.latestShipDate[type=hidden]").isEmpty() ? "" : row.select("input.latestShipDate[type=hidden]").val();
			latestShipDate = StringUtils.isNotBlank(latestShipDate) ? new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(Long.valueOf(latestShipDate) * 1000)) : "";
			boolean isPrime = (row.select("input.isPrime[type=hidden]") == null || "0".equals(row.select("input.isPrime[type=hidden]").val())) ? false : true;
			String numberOfItemsRemainingToShip = row.select("input.numberOfItemsRemainingToShip[type=hidden]").isEmpty() ? null : row.select("input.numberOfItemsRemainingToShip[type=hidden]").val();

			String orderDate = row.select("td").isEmpty() ? "" : row.select("td").eq(1).text();
			String title = row.select("td div[id*=orderItem] span[id*=product]").isEmpty() ? "" : row.select("td div[id*=orderItem] span[id*=product]").text();
			Elements qtyEle = row.select("td div[id*=orderItem] td:contains(QTY)").isEmpty() ? null : row.select("td div[id*=orderItem] td:contains(QTY)");
			int qty = 0;
			for (Element tmp: qtyEle) {
				qty += tmp != null ? Integer.valueOf(tmp.text().replaceAll("[^\\d.]", "")) : 0;
			}
			String asin = row.select("td div[id*=orderItem] td:contains(ASIN)").isEmpty() ? "" : row.select("td div[id*=orderItem] td:contains(ASIN)").text().replaceAll("ASIN:", "").trim();
			String sku = row.select("td div[id*=orderItem] td:contains(SKU)").isEmpty() ? "" : row.select("td div[id*=orderItem] td:contains(SKU)").text().replaceAll("SKU:", "").trim();
			String buyerName = row.select("td a[id*=buyerName]").isEmpty() ? "" : row.select("td a[id*=buyerName]").text();
			String shipping = row.select("td[class*=order-cell]").isEmpty() ? "" : row.select("td[class*=order-cell]").eq(3).text();
			String status = row.select("td[class*=order-cell]").isEmpty() ? "" : row.select("td[class*=order-cell]").eq(4).select("div:not([style=display:none])").text();
			return new Orders(id, asin, sku, qty, title, eslevel, shipped, shipping, buyerId, buyerName, merchFulfilled, marketplaceId, latestShipDate, isPrime,
					Integer.valueOf(numberOfItemsRemainingToShip), orderDate, status);

		} catch (Exception e) {
			e.printStackTrace();
			log.error(log.getName() + " : program error: " + e);
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
		DruidDataSource dataSource = (DruidDataSource) context.getBean("dataSource");
		DBUtil.setDataSource(dataSource);
		context.start();
		System.err.println("starting......");
		OrderTest.timer();
		System.in.read();
	}
}
